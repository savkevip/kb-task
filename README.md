This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# usage:
- npm i
- npm start

# powered with:

- React
- Redux
- Ramda
- Styled Components
- React Router v4
- Thunk / Axios
