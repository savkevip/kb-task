import { map, prop, indexBy, evolve, merge, flip, always, assoc } from 'ramda';

export function setPropValue(state, prop, value) {
  return assoc(prop, value, state);
}

export function getData(state, type, items) {
  const ids = map(prop('id'), items.results);
  const lookUp = indexBy(prop('id'), items.results);

  return evolve({
    loading: always(false),
    pageSize: always(items.total_pages),
    [type]: evolve({
      ids: always(ids),
      lookUp: flip(merge)(lookUp)
    })
  }, state);
}
