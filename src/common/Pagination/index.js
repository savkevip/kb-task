import React from 'react';
import { range} from 'ramda';
import styled from 'styled-components';

const PaginationWrapper = styled.ul`
  display: flex;
  flex-wrap: wrap;
  list-style: none;
`;

const Item = styled.li`
  background-color: ${props => props.active ? '#474F85' : '#fff'};
  color: ${props => props.active ? '#fff' : '#474F85'};
  cursor: pointer;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color .3s;
  border: 1px solid #474f85;
`;

const Pagination = ({ pageSize, currentPage, onPageChange }) => {
  if (pageSize === 1) return null;
  const pages = range(1, pageSize + 1);

  return (
    <nav>
      <PaginationWrapper>
        {pages.map(page => (
          <Item
            key={page}
            active={page === currentPage}
            onClick={() => onPageChange(page)}
          >
            {page}
          </Item>
        ))}
      </PaginationWrapper>
    </nav>
  );
};

export default Pagination;
