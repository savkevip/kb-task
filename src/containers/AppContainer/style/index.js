import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const AppWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  font-family: sans-serif;
`;

export const AppTitle = styled.h1`
  color: #474F85;
`;

export const SearchWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 60%;
`;

export const SearchInput = styled.input`
  width: 100%;
  height: 35px;
  padding: 0 5px;
  background-color: #474F85;
  border: 0;
  color: #fff;
  &::-webkit-input-placeholder {
    color: #fff;
  }
  &::selection {
    background: #f3ecd3;
  }
`;

export const InputLabel = styled.span`
  width: 60%;
  color: #474f85;
  margin-bottom: 5px;
`;

export const SearchBtn = styled.button`
  width: 150px;
  height: 35px;
  display: flex;
  margin-left: 15px;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  background-color: #474f85;
  color: #fff;
  &:hover {
    background-color: #6d76b3;
  }
  &:disabled {
    background-color: #bfbfc1;
  }
`;

export const ResultsItem = styled(Link)``;

export const ResultsWrapper = styled.div`
  display: flex;
	flex-direction: column;
	flex-wrap: wrap;
	height: 100vw;
	width: 100%;
  margin-top: 10px;
  padding: 5px;
  & ${ResultsItem} {
    transition: .8s opacity;
     width: 33.3%;
  }
  &:hover ${ResultsItem} {
    opacity: 0.3;
  }
  &:hover ${ResultsItem}:hover {
    opacity: 1;
  } 
`;

export const NoItems = styled.span`
  display: flex;
  justify-content: center;
`;

export const ItemImg = styled.img`
  width: 100%;
  height: 100%;
`;

export const SelectWrapper= styled.div`
  width: 60%;
  display: flex;
  justify-content: flex-end;
  margin: 20px;
`;

export const SelectFilter = styled.select``;
