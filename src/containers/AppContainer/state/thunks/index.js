import { searchPhotosAPI } from '../api';
import {
  getPhotosStart,
  getPhotosSuccess,
  getPhotosFail
} from '../actions';

export function getPhotos(data) {
  return dispatch => {
    dispatch(getPhotosStart());
    searchPhotosAPI(data)
      .then(response => dispatch(getPhotosSuccess(response.data)))
      .catch(error => dispatch(getPhotosFail(error)));
  };
}
