import {
  GET_PHOTOS_START,
  GET_PHOTOS_SUCCESS,
  GET_PHOTOS_FAIL
} from '../constants/actionTypes';

export const getPhotosStart = data => ({
  type: GET_PHOTOS_START,
  data
});

export const getPhotosSuccess = data => ({
  type: GET_PHOTOS_SUCCESS,
  data
});

export const getPhotosFail = data => ({
  type: GET_PHOTOS_FAIL,
  data
});
