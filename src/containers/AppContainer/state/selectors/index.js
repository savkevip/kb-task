import { createSelector } from 'reselect';

export const getState = state => state.photos;

export const createCollectionSelector = () => createSelector(
  [getState],
  state =>
    state.photos.ids
      ? state.photos.ids.map(id => state.photos.lookUp[id]) : null
);
