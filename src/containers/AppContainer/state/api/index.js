import { api, clientId } from '../../../../constants';

export const searchPhotosAPI = data =>
  api.get('/search/photos/', data);
