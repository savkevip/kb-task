import {
  GET_PHOTOS_START,
  GET_PHOTOS_SUCCESS,
  GET_PHOTOS_FAIL
} from '../constants/actionTypes';
import { setPropValue, getData } from '../../../../utils';

const initState = {
  loading: false,
  pageSize: null,
  photos: {
    ids: null,
    lookUp: null
  }
};

const appReducer = (state = initState, action) => {
  switch (action.type) {
    case GET_PHOTOS_START:
      return setPropValue(state, 'loading', true);
    case GET_PHOTOS_SUCCESS:
      return getData(state, 'photos', action.data);
    case GET_PHOTOS_FAIL:
      return setPropValue(state, 'loading', false);
    default:
      return state;
  }
};

export default appReducer;
