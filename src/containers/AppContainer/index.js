import React, { Component } from 'react';
import { connect } from 'react-redux';
import Pagination from '../../common/Pagination';
import { getPhotos } from './state/thunks';
import { getState, createCollectionSelector } from './state/selectors';
import { clientId, filters, itemsSize } from '../../constants';
import { pickBy, identity } from 'ramda';
import {
  AppWrapper,
  AppTitle,
  SearchInput,
  InputLabel,
  ResultsWrapper,
  ResultsItem,
  NoItems,
  ItemImg,
  SearchBtn,
  SearchWrapper,
  SelectWrapper,
  SelectFilter
} from './style';

class App extends Component {

  state = {
    query: null,
    currentPage: 1,
    orientation: null
  };

  handleText = e =>
    this.setState({
      [e.target.name]: e.target.value
    });

  handleSearch = () => {
    const { query, currentPage, orientation } = this.state;
    const data = pickBy(identity, {
      query,
      page: currentPage,
      orientation,
      per_page: itemsSize,
      client_id: clientId
    });
    this.props.getPhotos(data);
  };

  handlePageChange = page =>
    this.setState({
      currentPage: page
    }, () => this.handleSearch());

  handleSelect = e =>
    this.setState({
      orientation: e.target.value !== 'Sort By' && e.target.value
    }, () => this.handleSearch());

  render() {
    const { photos, pageSize } = this.props;
    const { currentPage, query } = this.state;

    return (
      <AppWrapper>
        <AppTitle>Welcome To KroonStudio Image Search</AppTitle>
        <InputLabel>Hey, try typing something...</InputLabel>
        <SearchWrapper>
          <SearchInput onChange={this.handleText} name="query" type="text" placeholder="Search..." />
          <SearchBtn disabled={!query} onClick={this.handleSearch}>Search</SearchBtn>
        </SearchWrapper>
        <SelectWrapper>
          <SelectFilter disabled={!query} onChange={this.handleSelect}>
          {filters.map(filter =>
              <option key={filter.id} value={filter.value}>
                {filter.text}
              </option>)}
          </SelectFilter>
        </SelectWrapper>
        { photos && (
          <React.Fragment>
            <ResultsWrapper>
              {photos.length > 0 ? photos.map(photo =>
                <ResultsItem to={`/${photo.id}`} key={photo.id}>
                  <ItemImg src={photo.urls.small} />
                </ResultsItem>) :
                <NoItems>{`Sorry, we couldn't find any results matching...`}</NoItems>
              }
            </ResultsWrapper>
            <Pagination
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={this.handlePageChange}
            />
          </React.Fragment>
        ) }
      </AppWrapper>
    );
  }
}

const mapStateToProps = state => ({
  photos: createCollectionSelector()(state),
  loading: getState(state).loading,
  pageSize: getState(state).pageSize,
});

const mapDispatchToProps = dispatch => ({
  getPhotos: data => dispatch(getPhotos(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
