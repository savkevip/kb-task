import {
  GET_DETAILS_START,
  GET_DETAILS_SUCCESS,
  GET_DETAILS_FAIL
} from '../constants/actionTypes';
import { setPropValue } from '../../../../utils';
import { evolve, always } from 'ramda';

const initState = {
  loading: false,
  photo: null
};

const detailsReducer = (state = initState, action) => {
  switch (action.type) {
    case GET_DETAILS_START:
      return setPropValue(state, 'loading', true);
    case GET_DETAILS_SUCCESS:
      return evolve({ photo: always(action.data), loading: always(false) }, state);
    case GET_DETAILS_FAIL:
      return setPropValue(state, 'loading', false);
    default:
      return state;
  }
};

export default detailsReducer;
