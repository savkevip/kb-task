import { clientId, api } from '../../../../constants';

export const getDetailsAPI = id =>
  api.get(`photos/${id}/?client_id=${clientId}`);
