import {
  GET_DETAILS_FAIL,
  GET_DETAILS_START,
  GET_DETAILS_SUCCESS
} from "../constants/actionTypes";

export const getDetailsStart = data => ({
  type: GET_DETAILS_START,
  data
});

export const getDetailsSuccess = data => ({
  type: GET_DETAILS_SUCCESS,
  data
});

export const getDetailsFail = data => ({
  type: GET_DETAILS_FAIL,
  data
});
