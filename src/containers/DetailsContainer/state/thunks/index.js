import { getDetailsAPI } from '../api';
import {
  getDetailsStart,
  getDetailsSuccess,
  getDetailsFail
} from '../actions';

export function getDetails(id) {
  return dispatch => {
    dispatch(getDetailsStart());
    getDetailsAPI(id)
      .then(response => dispatch(getDetailsSuccess(response.data)))
      .catch(error => dispatch(getDetailsFail(error)));
  };
}
