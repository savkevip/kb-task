import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const DetailsWrapper = styled.div`
  font-family: sans-serif;
  padding: 20px;
`;

export const Content = styled.div`
  display: flex;
`;

export const LeftContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background-color: #474f85;
  padding: 20px;
  color: #fff;
  width: 20%;
`;

export const RightContent = styled.div`
  width: 80%;
`;

export const ContentDetails = styled.div`
  display: flex;
  flex-direction: column;
`;

export const BackBtn = styled(Link)`
  display: flex;
  justify-content: center;
  text-decoration: none;
  background-color: #51e3d4; 
  color: #fff;
  padding: 10px 5px;
  &:hover {
    background-color: #3fb7ab; 
  }
`;

export const Img = styled.img`
  width: 100%;
  height: 100%;
`;
