import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getDetails } from './state/thunks';
import { getState } from './state/selectors';
import Loader from '../../common/Loader';
import {
  DetailsWrapper,
  Content,
  LeftContent,
  RightContent,
  ContentDetails,
  BackBtn,
  Img
} from './style';

class Details extends Component {

  componentDidMount() {
    const { getDetails, match: { params } } = this.props;
    getDetails(params.id);
  }


  render() {
    const { photo, loading } = this.props;
    if (!photo) return null;

    return (
      <DetailsWrapper>
        {loading ? <Loader /> : (
          <Content>
            <LeftContent>
              <ContentDetails>
                <span>{ photo.user && `Taken By: ${photo.user.name}` }</span>
                <span>{ photo.user && `Bio: ${photo.user.bio}` }</span>
                <span>{ photo.location && `Location ${photo.location.title}` }</span>
                <span>{ photo.downloads && `Downloads: ${photo.downloads}` }</span>
                <span>{ photo.user && `Total Likes: ${photo.user.total_likes}` }</span>
              </ContentDetails>
              <BackBtn to="/">BACK</BackBtn>
            </LeftContent>
            <RightContent>
              <Img src={photo.urls.regular} />
            </RightContent>
          </Content>
        )}
      </DetailsWrapper>
    );
  }
}

const mapStateToProps = state => ({
  photo: getState(state).photo,
  loading: getState(state).loading
});

const mapDispatchToProps = dispatch => ({
  getDetails: data => dispatch(getDetails(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Details);
