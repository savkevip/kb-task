import { combineReducers } from 'redux';
import appReducer from './containers/AppContainer/state/reducer';
import detailsReducer from './containers/DetailsContainer/state/reducer';

export default combineReducers({
  photos: appReducer,
  photo: detailsReducer
});
