import { create } from 'apisauce';

export const filters = [
  { value: null, text: 'Sort By', id: 1 },
  { value: 'landscape', text: 'Landscape',  id: 2 },
  { value: 'portrait', text: 'Portrait', id: 3 },
  { value: 'squarish', text: 'Squarish', id: 4}
];

export const itemsSize = 9;

export const clientId = '8e804f6838fd3e00af6c4fd9c97f1212d49e793a0ec9c230d9767f43bc7a8758';

const API_URL = 'https://api.unsplash.com';

export const api = create({
  baseURL: API_URL
});
